jQuery(document).ready(function(){
    jQuery('input[name=name]').change(function() {
        jQuery("input[name=name]").css('border','1px #CCC solid');
        jQuery("#name_error").text("");
    });
    jQuery('input[name=email]').change(function() {
        jQuery("input[name=email]").css('border','1px #CCC solid');
        jQuery("#email_error").text("");
    });
    jQuery('input[name=mobile]').change(function() {
        jQuery("input[name=mobile]").css('border','1px #CCC solid');
        jQuery("#mobile_error").text("");
    });
    jQuery('textarea[name=address]').change(function() {
        jQuery("#address").css('border','1px #CCC solid');
        jQuery("#address_error").text("");
    });
    jQuery('textarea[name=reason]').change(function() {
        jQuery("#reason").css('border','1px #CCC solid');
        jQuery("#reason_error").text("");
    });
    jQuery("#register_form").click(function() {
        var ok=true;

        var Name=jQuery('input[name=name]').val().trim();
        if(Name==""){
            jQuery("input[name=name]").css('border','1px red solid');
            jQuery("#name_error").show();
            jQuery("#name_error").text('Name is required');
            ok=false;
        }
        else if(!Name.match(/^([a-zA-Z._\s])+$/i)) {
            jQuery("input[name=name]").css('border','1px red solid');
            jQuery("#name_error").show();
            jQuery("#name_error").text('Only alphabets and spaces are allowed');
            ok=false;
        }

        var Email=jQuery('input[name=email]').val().trim();
        var Useremailfilter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if(Email==""){
            jQuery("input[name=email]").css('border','1px red solid');
            jQuery("#email_error").show();
            jQuery("#email_error").text('Email is required');
            ok=false;

        } else if(!Useremailfilter.test(Email)){
            jQuery("input[name=email]").css('border','1px red solid');
            jQuery("#email_error").show();
            jQuery("#email_error").text('Please enter proper Email');
            ok=false;
        }
        var Mobile=jQuery('input[name=mobile]').val().trim();
        if(Mobile==""){
            jQuery("input[name=mobile]").css('border','1px red solid');
            jQuery("#mobile_error").show();
            jQuery("#mobile_error").text('Mobile num is required');
            ok=false;

        } else if(!Mobile.match(/^[789]\d{9}$/)){
            jQuery("input[name=mobile]").css('border','1px red solid');
            jQuery("#mobile_error").show();
            jQuery("#mobile_error").text('Please enter proper Mobile Number');
            ok=false;
        }
        var Address = jQuery('textarea[name=address]').val().trim();
        if(Address==""){
            jQuery("textarea[name=address]").css('border','1px red solid');
            jQuery("#address_error").show();
            jQuery("#address_error").text('Address is required');
            ok=false;
        }

        var Reason=jQuery('textarea[name=reason]').val().trim();
        if(Reason==""){
            jQuery("textarea[name=reason]").css('border','1px red solid');
            jQuery("#reason_error").show();
            jQuery("#reason_error").text('Reason is required');
            ok=false;
        }

        if(ok==true) {
            jQuery.ajax({
                type: "POST",
                data: 'name='+ Name + '&email='+ Email + '&mobile='+ Mobile + '&reason='+ Reason + '&address=' + Address,
                url: "sendmail.php",
                success: function(data)
                {
                    jQuery("#message").show();
                    if (data.indexOf('Registered Successfully') > -1) {
                        jQuery("#message").text(data).css('color','green');
                        setTimeout(function(){ jQuery('#message').fadeOut() }, 60000);
                        jQuery("#registerSubmit").find('input:text, input:password, input:file, select, textarea').val('');

                    } else {
                        jQuery("#message").text(data).css('color','red');
                    }
                }
            });
        }
        return ok;
    });

});

jQuery(function() {
    jQuery.support.placeholder = false;
    p = document.createElement('form');
    if('placeholder' in p) jQuery.support.placeholder = true;
});

jQuery(function() {
    if(!jQuery.support.placeholder) {
        var active = document.activeElement;
        jQuery('input[type="text"], textarea').focus(function () {
            if (jQuery(this).attr('placeholder') != '' && jQuery(this).val() == jQuery(this).attr('placeholder')) {
                jQuery(this).val('').removeClass('hasPlaceholder');
            }
        }).blur(function () {
            if (jQuery(this).attr('placeholder') != '' && (jQuery(this).val() == '' || jQuery(this).val() == jQuery(this).attr('placeholder'))) {
                jQuery(this).val(jQuery(this).attr('placeholder')).addClass('hasPlaceholder');
            }
        });
        jQuery('input[type="text"], textarea').blur();
        jQuery(active).focus();
        jQuery('form:eq(0)').submit(function () {
            jQuery('input[type="text"].hasPlaceholder, textarea.hasPlaceholder').val('');
        });
    }
});
