/* Style Switcher */
window.console = window.console || (function($){
	var c = {};
	c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function(){};
	return c;
})();

jQuery(document).ready(function($){

	var styleswitcherstr = ' \
		<h2>Style Switcher <a href="#"></a></h2> \
	    <div class="content"> \
	    <h3>Top Bar Style</h3> \
		<div class="layout-switcher"> \
			<a id="def" class="layout">Default</a> \
			<a id="base" class="layout">Base</a> \
			<a id="dark" class="layout">Dark</a> \
			<a id="gray" class="layout">gray</a> \
	    </div> \
	    \
	    <div class="clear"></div> \
	    <h3>Breadcrumbs Style</h3> \
		<div class="breadcrumbs-switcher"> \
			<a id="style1" class="layout">Style 1</a> \
			<a id="style2" class="layout">Style 2</a> \
			<a id="style3" class="layout">Style 3</a> \
	    </div> \
	    \
	    <div class="clear"></div> \
		<h3>Change Color</h3> \
	    <div class="switcher-box"> \
			<a id="default" class="styleswitch color"></a> \
			<a id="green" class="styleswitch color"></a> \
			<a id="purple" class="styleswitch color"></a> \
			<a id="dpurple" class="styleswitch color"></a> \
			<a id="orange" class="styleswitch color"></a> \
			<a id="dorange" class="styleswitch color"></a> \
			<a id="brown" class="styleswitch color"></a> \
			<a id="teal" class="styleswitch color"></a> \
			<a id="pink" class="styleswitch color"></a> \
			<a id="red" class="styleswitch color"></a> \
			<a id="blugrey" class="styleswitch color"></a> \
			<a id="amber" class="styleswitch color"></a> \
	    </div><!-- End switcher-box --> \
	    <div class="clear"></div> \
	    </div><!-- End content --> \
		';

	$(".switcher").prepend( styleswitcherstr );

});

/* TopBar Style */
jQuery(document).ready(function($){
	var cookname = 'def';

	function changeTopBar(layout) {
		$.cookie(cookname, layout);
		// $('head link[data-name=layout]').attr('href', 'css/layout/' + layout + '.css');
		$('.header-top').addClass(layout);
	}

	if( $.cookie(cookname)) {
		changeTopBar($.cookie(cookname));
	}

	$("#def").click( function(){ $
		$('.header-top').removeClass('base');
		$('.header-top').removeClass('dark');
		$('.header-top').removeClass('gray');
		changeTopBar('def');
	});

	$("#dark").click( function(){ $
		$('.header-top').removeClass('def');
		$('.header-top').removeClass('base');
		$('.header-top').removeClass('gray');
		changeTopBar('dark');
	});

	$("#base").click( function(){ $
		$('.header-top').removeClass('def');
		$('.header-top').removeClass('dark');
		$('.header-top').removeClass('gray');
		changeTopBar('base');
	});

	$("#gray").click( function(){ $
		$('.header-top').removeClass('def');
		$('.header-top').removeClass('dark');
		$('.header-top').removeClass('base');
		changeTopBar('gray');
	});

});


/* Breadcrumbs Style*/
jQuery(document).ready(function($){
	var cookname = 'default';

	function changeBreadcrumbs(layout) {
		$.cookie(cookname, layout);
		// $('head link[data-name=layout]').attr('href', 'css/layout/' + layout + '.css');
		$('.banner-breadcrumb').addClass(layout);
	}

	if( $.cookie(cookname)) {
		changeBreadcrumbs($.cookie(cookname));
	}

	$("#style1").click( function(){ $
		$('.banner-breadcrumb').removeClass('style1');
		$('.banner-breadcrumb').removeClass('style2');
		changeBreadcrumbs('default');
	});

	/*Linear BG*/
	$("#style2").click( function(){ $
		$('.banner-breadcrumb').removeClass('default');
		$('.banner-breadcrumb').removeClass('style2');
		changeBreadcrumbs('style1');
	});

	/*Img BG*/
	$("#style3").click( function(){ $
		$('.banner-breadcrumb').removeClass('default');
		$('.banner-breadcrumb').removeClass('style1');
		changeBreadcrumbs('style2');
	});

});


/* Skins Style */
jQuery(document).ready(function($){
	var cookieName = 'default';

	function changeLayout(layout) {
		$.cookie(cookieName, layout);
		$('head link[data-name=skins]').attr('href', 'assets/stylesheets/skins/' + layout + '.css');
	}

	if( $.cookie(cookieName)) {
		changeLayout($.cookie(cookieName));
	}

	$("#default").click( function(){ $
		changeLayout('default');
	});

	$("#green").click( function(){ $
		changeLayout('green');
	});
	$("#purple").click( function(){ $
		changeLayout('purple');
	});
	$("#dpurple").click( function(){ $
		changeLayout('dpurple');
	});
	$("#orange").click( function(){ $
		changeLayout('orange');
	});
	$("#dorange").click( function(){ $
		changeLayout('dorange');
	});
	$("#brown").click( function(){ $
		changeLayout('brown');
	});
	$("#teal").click( function(){ $
		changeLayout('teal');
	});
	$("#pink").click( function(){ $
		changeLayout('pink');
	});
	$("#red").click( function(){ $
		changeLayout('red');
	});
	$("#blugrey").click( function(){ $
		changeLayout('blugrey');
	});
	$("#amber").click( function(){ $
		changeLayout('amber');
	});

});


/* Reset Switcher */
jQuery(document).ready(function($){
	// Style Switcher
	$('.switcher').animate({
		left: '-255px'
	});

	$('.switcher h2 a').click(function(e){
		e.preventDefault();
		var div = $('.switcher');
		if (div.css('left') === '-255px') {
			$('.switcher').animate({
		  	left: '0px'
			});
		} else {
			$('.switcher').animate({
		  	left: '-255px'
			});
		}
	})
});

