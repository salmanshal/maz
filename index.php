<!DOCTYPE html>
<html lang="en" class="no-js">
  
<!-- Mirrored from jogjafile.com/html/bala/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 08 Mar 2017 21:27:18 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>MAZ Global Servies</title>
    <link href="assets/stylesheets/vendor/bootstrap.css" rel="stylesheet" media="all" type="text/css">
    <link href="assets/stylesheets/vendor/ionicons.css" rel="stylesheet" media="all" type="text/css">
    <link href="assets/stylesheets/modules/animate.css" rel="stylesheet" media="all" type="text/css">
    <link href="assets/stylesheets/modules/flexslider.css" rel="stylesheet" media="all" type="text/css">
    <link href="assets/js/rs-plugin/css/settings.css" rel="stylesheet" media="all" type="text/css">
    <link href="assets/stylesheets/style.css" rel="stylesheet" media="all" type="text/css">
    <link href="assets/stylesheets/style.css" rel="stylesheet" media="all" data-name="skins" type="text/css">
    <link href="assets/stylesheets/switcher.css" rel="stylesheet" media="all" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css"><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script> <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.2/jquery.xdomainrequest.min.js"></script>
    <![endif]-->
  </head>
  <body class="fixHeader">
    <div id="preloader"><span></span><span></span><span></span><span></span><span></span></div>
    <header id="main_header" class="default clearfix">
      <div id="header-default" class="main-header">
        <div class="header-top">
          <div class="container">
            <div class="row">
              <div class="col-md-9 clearfix">
                <div class="contact-bar">
                                <ul class="contact-top border">
                                  <li>
                                    <p><i class="ion-android-mail"></i><span>Email:</span><a href="#">info@mazglobalservices.com</a></p>
                                  </li>
                                    <li>
                                        <p><i class="ion-android-mail"></i><span>Phone:</span><a
                                                href="#">044 48513690</a></p>
                                    </li>
                                  <li class="hidden-xs">
                                    <p><span><a href="http://mazglobalservices.com/user/index.php?controller=authentication&back=my-account" target="_blank">LOGIN</a></span></p>
                                  </li>
                                </ul>
                </div>
              </div>
              <div class="col-md-2 hidden-xs hidden-sm">
                <div class="social-bar pull-right">
                                <ul class="social ion-fonts">
                                  <li><a href="#" data-placement="bottom" data-toggle="tooltip" data-original-title="facebook" title="facebook" class="facebook"><i class="ion-social-facebook"></i></a></li>
                                  <li><a href="#" data-placement="bottom" data-toggle="tooltip" data-original-title="twitter" title="twitter" class="twitter"><i class="ion-social-twitter"></i></a></li>
                             <!--     <li><a href="#" data-placement="bottom" data-toggle="tooltip" data-original-title="github" title="github" class="github"><i class="ion-social-github"></i></a></li>
                                  <li><a href="#" data-placement="bottom" data-toggle="tooltip" data-original-title="youtube" title="youtube" class="youtube"><i class="ion-social-youtube"></i></a></li>
                                  <li><a href="#" data-placement="bottom" data-toggle="tooltip" data-original-title="linkedin" title="linkedin" class="linkedin"><i class="ion-social-linkedin"></i></a></li>
                                  <li><a href="#" data-placement="bottom" data-toggle="tooltip" data-original-title="pinterest" title="pinterest" class="pinterest"><i class="ion-social-pinterest"></i></a></li>
                                  <li><a href="#" data-placement="bottom" data-toggle="tooltip" data-original-title="skype" title="skype" class="skype"><i class="ion-social-skype"></i></a></li>-->
                                </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="header-menu">
          <div class="container">
            <div id="menu_toggle_button"><i class="ion-navicon-round"></i></div>
            <div id="logo"><a href="index.php"><img src="assets/img/logo.png" alt="logos"></a></div>
            <div class="search-toggle"><a title="Search" class="search-icon"><i class="ion-search"></i></a>
              <div id="search-container" class="search-box-wrapper">
                <form name="search-form" class="search-menu">
                  <input name="search-input" placeholder="Enter your search..." type="text">
                </form>
              </div>
            </div><nav class="main-nav pull-right">
                <ul id="main_menu" class="menu clearfix">
                
               <!-- <li class="current-menu-item"><a href="index.php">home</a>-->
                    
                </li>
                <li><a href="#">Startup</a>
                    <div class="megaMenu three-quarter">
                        <div class="megaMenu-inner clearfix">
                            <ul>
                                <li><span>Business Registration</span>
                                    <ul>
                                        <li><a href="esi.php">ESI Registration</a></li>
                                        <li><a href="gst.php">GST Registration</a></li>
                                        <li><a href="ie.php">IE Code Registration</a></li>
                                        <li><a href="pf.php">PF Registration</a></li>
                                        <li><a href="tax.php">Professional TAX Registration</a></li>
                                        <li><a href="st.php">ST Registration</a></li>
                                        <li><a href="tin.php">TIN Registration</a></li>
                                        <li><a href="trademark.php">Trademark Registration</a></li>
                                    </ul>
                                </li>
                                <li><span>FIRM Registration</span>
                                    <ul>
                                        <li><a href="llp.php">LLP Registration</a></li>
                                        <li><a href="opc.php">OPC Registration</a></li>
                                        <li><a href="partnership.php">Partnership Registration</a></li>
                                        <li><a href="pvt.php">Private Limited Registration</a></li>
                                        <li><a href="society.php">Society Registration</a></li>
                                        <li><a href="ssi.php">SSI or MSME Registration</a></li>
                                        <li><a href="trust.php">Trust Registration</a></li>
                                    </ul>
                                </li>
                                <li><span>Licensing Services</span>
                                    <ul>
                                        <li><a href="apenda.php">Apeda Registration</a></li>
                                        <li><a href="dsc.php">DSC Registration</a></li>
                                        <li><a href="fire.php">Fire & Safety Registration</a></li>
                                        <li><a href="fssai.php">FSSAI License</a></li>
                                        <li><a href="spice.php">Spice Board Registration</a></li>
                                        <li><a href="tea.php">Tea Board Registration</a></li>
                                        <li><a href="trade.php">Trade License</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
              <li><a href="#">Accounting & Complaince</a>
                <ul class="navi">
                        <li><a href="add.php">Add Directors or Partner or Subscriber</a></li>
                        <li><a href="auditor.php">Auditor Appointment</a></li>
                        <li><a href="winding.php">Dissolution or Winding Up</a></li>
                        <li><a href="capital.php">Increase Authorized Capital</a></li>
                        <li><a href="name_change.php">Name Change</a></li>
						<li><a href="register_office.php">Register Office Change</a></li>
						<li><a href="remove.php">Remove Directors or Partner or Subscriber</a></li>
						<li><a href="share_transfer.php">Share Transfer</a></li>
                      </ul>
              </li>
                <li ><a href="#">IXBRL</a>

                    <ul>

                        <li><a href="india_xbrl.php">India XBRL</a></li>
                        <li><a href="singapore_xbrl.php">Singapore XBRL</a></li>


                    </ul>

                </li>
              <li><a href="#">Legal Consulting</a>
                <!--  <ul>
                    <li><a href="contact.html">Style 1</a></li>
                    <li><a href="contact-2.html">Style 2</a></li>
                  </ul>-->
              </li>
              <li><a href="#">Software Solutions</a></li>
            </ul></nav>
          </div>
        </div>
      </div>
    </header>
    
    
    <section class="slider">
      <div class="fullwidthbanner-container">
        <div class="tp-banner">
          <ul>
            <li data-transitions="fade" data-slotamount="5" data-masterspeed="800" data-fsmasterspeed="800" data-fsslotamount="5" data-saveperformance="on" data-thumb="assets/img/revslider/bg_3.jpg" data-title="slider 1"><img src="assets/img/revslider/transparent.png" data-lazyload="assets/img/revslider/bg_1.jpg" alt="slider img" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat"/>
              <div data-x="610" data-y="0" data-speed="600" data-start="1000" data-easing="easeOutExpo" data-endspeed="1000" data-endeasing="Power3.easeInOut" class="tp-caption sfr"><img src="assets/img/revslider/transparent.png" alt="slider images" data-lazyload="assets/img/revslider/people_2.png"/></div>
              <div data-x="40" data-y="140" data-hoffset="0" data-speed="500" data-start="1400" data-easing="easeOutExpo" data-endspeed="1000" data-endeasing="easeInSine" class="tp-caption custom_title fz_50 bold white sfr uppercase">MAZ GLOBAL SERVICES</div>
              <div data-x="40" data-y="190" data-hoffset="0" data-speed="500" data-start="1800" data-easing="easeOutExpo" data-endspeed="500" data-endeasing="easeInSine" class="tp-caption custom_title fz_35 white sfr uppercase bolder"></div>
              <div data-x="40" data-y="257" data-hoffset="0" data-speed="2000" data-start="2200" data-easing="Back.easeInOut" data-endspeed="300" class="tp-caption custom_text white fw_600 sfb">
                  We provide three major services. We help in setting up a business,<br> we do accounting and compliance services
              </div>
             <!-- <div data-x="40" data-y="357" data-hoffset="0" data-speed="1000" data-start="2600" data-easing="Back.easeInOut" data-endspeed="300" class="tp-caption tp-resizeme sfb"><a href="#" class="btn btn-bg-default uppercase bold">Purchase now</a></div>
              <div data-x="220" data-y="357" data-hoffset="0" data-speed="1000" data-start="2800" data-easing="Back.easeInOut" data-endspeed="300" class="tp-caption tp-resizeme sfb"><a href="#" class="btn btn-line-white uppercase bold">more features</a></div>-->
            </li>
            <li data-transitions="fade" data-slotamount="5" data-masterspeed="700" data-fsmasterspeed="700" data-fsslotamount="5" data-saveperformance="on" data-thumb="assets/img/revslider/bg_3.jpg" data-title="slider 2"><img src="assets/img/revslider/transparent.png" data-lazyload="assets/img/revslider/bg_3.jpg" alt="slider img" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat"/>
              <div data-x="20" data-y="140" data-hoffset="0" data-speed="2000" data-start="1500" data-easing="Back.easeInOut" data-endspeed="300" class="tp-caption tp-resizeme custom_text white fw_600 sfb">
                <div class="icons-feature white rounded"><a href="#" class="icons-feature-head"><i class="ion-social-html5"></i></a><a href="#" class="icons-feature-title">
                    <h3>Powerful & Clean Code</h3></a></div>
              </div>
              <div data-x="20" data-y="250" data-hoffset="0" data-speed="2000" data-start="1700" data-easing="Back.easeInOut" data-endspeed="300" class="tp-caption custom_text white fw_600 sfb tp-resizeme">
                <div class="icons-feature white rounded"><a href="#" class="icons-feature-head"><i class="ion-help-buoy"></i></a><a href="#" class="icons-feature-title">
                    <h3>Friendly Support</h3></a></div>
              </div>
              <div data-x="400" data-y="140" data-hoffset="0" data-speed="2000" data-start="1900" data-easing="Back.easeInOut" data-endspeed="300" class="tp-caption custom_text white fw_600 sfb tp-resizeme">
                <div class="icons-feature white rounded"><a href="#" class="icons-feature-head"><i class="ion-monitor"></i></a><a href="#" class="icons-feature-title">
                    <h3>Responsive Bootstrap</h3></a></div>
              </div>
              <div data-x="400" data-y="250" data-hoffset="0" data-speed="2000" data-start="2100" data-easing="Back.easeInOut" data-endspeed="300" class="tp-caption custom_text white fw_600 sfb tp-resizeme">
                <div class="icons-feature white rounded"><a href="#" class="icons-feature-head"><i class="ion-social-css3"></i></a><a href="#" class="icons-feature-title">
                    <h3>CSS3 Animations</h3></a></div>
              </div>
              <div data-x="20" data-y="390" data-hoffset="0" data-speed="1000" data-start="2500" data-easing="Back.easeInOut" data-endspeed="300" class="tp-caption tp-resizeme sfb"><a href="#" class="btn btn-line-white uppercase bold">more features</a></div>

            </li>
            <li data-transitions="fade" data-slotamount="7" data-masterspeed="1000" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="on" data-thumb="assets/img/revslider/bg_2.jpg"><img src="assets/img/revslider/transparent.png" data-lazyload="assets/img/revslider/bg_2.jpg" alt="slider img" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat"/>
              <div data-x="550" data-y="180" data-speed="800" data-start="1500" data-easing="easeOutExpo" data-endspeed="1000" data-endeasing="Power3.easeInOut" class="tp-caption sfr"><img src="assets/img/revslider/transparent.png" alt="slider images" data-lazyload="assets/img/revslider/people_3.png"/></div>
              <div data-x="50" data-y="138" data-hoffset="0" data-speed="500" data-start="1000" data-easing="easeOutExpo" data-endspeed="1000" data-endeasing="easeInSine" class="tp-caption custom_title fz_30 white sfr"><span class="default capital bold"></span></div>
              <div data-x="20" data-y="165" data-hoffset="0" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1000" data-start="1500" data-easing="Back.easeInOut" data-endspeed="1000" data-endeasing="Power1.easeIn" class="tp-caption custom_title fz_50 bold white capital customin customout start">MAZ GLOBAL SERVICES</div>
              <div data-x="20" data-y="232" data-hoffset="0" data-speed="2000" data-start="2200" data-easing="Back.easeInOut" data-endspeed="300" class="tp-caption custom_text white fw_600 sfb">
                  MAZ is a global service provider of<br> XBRL Tagging, Accounting, Taxation, Registration, Legal and Web designing Services.
              </div>
              <div data-x="20" data-y="350" data-hoffset="0" data-speed="1000" data-start="2600" data-easing="Back.easeInOut" data-endspeed="300" class="tp-caption tp-resizeme sfb"><a href="#" class="btn btn-bg-default uppercase bold">Purchase Now</a></div>
              <div data-x="200" data-y="350" data-hoffset="0" data-speed="1000" data-start="2800" data-easing="Back.easeInOut" data-endspeed="300" class="tp-caption tp-resizeme sfb"><a href="#" class="btn btn-line-white uppercase bold">more Feature</a></div>
            </li>
          </ul>
        </div>
      </div>
    </section>
    <section class="main-content">
      <div class="container">
        <div class="row">
          <div class="heading" style="margin-left: 10%;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <h1>welcome to <span>MAZ Global Services</span></h1>
              <hr class="spacer"/>
              <p>MAZ is a global service provider of XBRL Tagging, Accounting, Taxation, Registration, Legal and Web designing Services.</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="service-box center style1 border">
              <div class="sb-icon-head">
                <div class="sb-icon"><i class="fa ion-laptop"></i></div>
              </div>
              <div class="sb-title">
                <h3>Who We Are</h3>
              </div>
              <p>We’re a team of young professionals who sincerely believe that the startups of today are going to change our
                tomorrow through technology and innovation that will enrich and improve our lives.
              </p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="service-box center style1 border">
              <div class="sb-icon-head">
                <div class="sb-icon"><i class="fa ion-settings"></i></div>
              </div>
              <div class="sb-title">
                <h3>WHAT DO WE DO?</h3>
              </div>
              <p>We provide three major services. We help in setting up a business, we do accounting and compliance services
                and we also help them scale by providing business consulting and financial services.
              </p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="service-box center style1 border">
              <div class="sb-icon-head">
                <div class="sb-icon"><i class="fa ion-archive"></i></div>
              </div>
              <div class="sb-title">
                <h3>WHAT DRIVES US?</h3>
              </div>
              <p>The fact that we provide our services to these startups and companies, means together with these startups,
                we are helping build a better tomorrow. Think of us as the pit stop crew in an F1 race.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  <!--  <section id="parallax" data-parallax="assets/img/parallax/03.jpg" class="main-content action-box big">
      <div class="container">
        <div class="col-md-12">
          <div class="action-box-content">
            <h1>easy to customize</h1>
            <p>Voluptate ipsam reprehenderit consequatur sint similique itaque, sunt tempora aspernatur veniam libero minus tempora reiciendis adipisci iet dolorem maxime aliquid minima quaerat dolorum quibusdam reiciendis, aspernatur veniam fugiat facere.</p><a href="#" class="btn btn-line-white">more feature</a><a href="#" class="btn btn-bg-white">purchase now</a>
          </div>
        </div>
      </div>
    </section>-->
    <section class="main-content action-box big bg-main">
        <div class="container">
            <div class="col-md-12">
                <div class="action-box-content">
                    <h1>Is your business GST Ready?</h1>
                    <h3>Goods and Services Tax (GST) is India’s biggest indirect tax reform in decades.</h3>
                    <p>GST will integrate India into one common market by removing the various tax and fiscal barriers between the states. IndiaFilings provides a suite of GST services like GST registration and GST return filing through its online accounting platform designed for SMEs. Checkout the IndiaFilings GST Portal and get your business GST ready today!</p><a href="#" class="btn btn-line-white">more feature</a><a href="#" class="btn btn-bg-white">GET STARTED</a>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row">
                <div class="heading">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-md-offset-12">
                        <h3>Welcome Offer</h3>
                        <hr class="spacer"/>
                        <!-- <p> Consectetur adipisicing elit. Placeat nulla assumenda reiciendis, maxime suscipit nihil error doloremque est aspernatur, sit ullam, aliquid ratione at? Commodi aperiam nostrum delectus saepe tenetur.</p>-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 fssai-license" style="
             border: 1px solid #dcd8d8;
              padding-top: 25px;">
                    <div class="counter_number">
                        <div class="head-icons"><img src="assets/img/fl.jpg"> </div><h3>Fssai License</h3>
                        <hr class="spacer"/>
                        <p>Starts From Rs.2,999/-*</p>
                        <a href="http://mazglobalservices.com/fssai.php">Know More</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 import-code" style="
            border: 1px solid #dcd8d8;
            padding-top: 25px;">
                    <div class="counter_number">
                        <div class="head-icons"><img src="assets/img/ie.jpg"></div><h3>Import Export Code</h3>
                        <hr class="spacer"/>
                        <p>Starts From Rs.2,499/-*</p>
                        <a href="http://mazglobalservices.com/ie.php">Know More</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 trademark-register" style="
             border: 1px solid #dcd8d8;
             padding-top: 25px;">
                    <div class="counter_number">
                        <div class="head-icons"><img src="assets/img/tr.jpg"></div><h3>Trademark Registration</h3>
                        <hr class="spacer"/>
                        <p>Starts From Rs.9,999/-*</p>
                        <a href="http://mazglobalservices.com/trademark.php">Know More</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 service-register" style="
             border: 1px solid #dcd8d8;
                padding-top: 25px;">
                    <div class="counter_number">
                        <div class="head-icons"><img src="assets/img/st.jpg"></div><h3>Service Tax Registration</h3>
                        <hr class="spacer"/>
                        <p>Starts From Rs.1,499/-*</p>
                        <a href="http://mazglobalservices.com/st.php">Know More</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 income-return" style="
             border: 1px solid #dcd8d8;
                padding-top: 25px;">
                    <div class="counter_number">
                        <div class="head-icons"><img src="assets/img/it.jpg"></div><h3>Income Tax Return</h3>
                        <hr class="spacer"/>
                        <p>Starts From Rs.1,499/-*</p>
                        <a href="http://mazglobalservices.com/tax.php">Know More</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 gst-register" style="
             border: 1px solid #dcd8d8;
                padding-top: 25px;">
                    <div class="counter_number">
                        <div class="head-icons"><img src="assets/img/pvt.jpg"></div><h3>PVT LTD Registration</h3>
                        <hr class="spacer"/>
                        <p>Starts From Rs.11,999/-*</p>
                        <a href="http://mazglobalservices.com/pvt.php">Know More</a>
                    </div>
                </div>
               <!-- <div class="col-md-3 col-sm-6 pvt-register" style="
             border: 1px solid #dcd8d8;
                padding-top: 25px;">
                    <div class="counter_number">
                        <div class="head-icons"><img src="assets/img/pvt.jpg"></div><h3>PVT LTD Registration</h3>
                        <hr class="spacer"/>
                        <p>Starts From Rs.11,999/-*</p>
                        <a href="http://mazglobalservices.com/pvt.php">Know More</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 llp-register" style="
             border:1px solid #dcd8d8;
                padding-top: 25px;">
                    <div class="counter_number">
                        <div class="head-icons"><img src="assets/img/pvt.jpg"></div><h3>LLP</h3>
                        <hr class="spacer"/>
                        <p>Starts From Rs.6,499/-*</p>
                        <a href="http://mazglobalservices.com/llp.php">Know More</a>
                    </div>
                </div>-->
            </div>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row">
                <div class="heading">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>fun facts</h3>
                        <hr class="spacer"/>
                        <p>We utilise a mix of technology and human touch to deliver quality business services, at scale across india. With a network of over 3000 professionals spread across over 170+ cities and towns, we have local reach while being highly accessible through the internet and smart phone.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="counter_number">
                        <div class="head-icons"><i class="ion-person-stalker"></i></div><span data-number="500" data-duration="1750" class="number_counter"></span>
                        <hr class="spacer"/>
                        <p>happy customers</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="counter_number">
                        <div class="head-icons"><i class="ion-connection-bars"></i></div><span data-number="100" data-duration="2000" class="number_counter"></span>
                        <hr class="spacer"/>
                        <p>total projects</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="counter_number">
                        <div class="head-icons"><i class="ion-speedometer"></i></div><span data-number="23983" data-duration="2500" class="number_counter"></span>
                        <hr class="spacer"/>
                        <p>Hours in work</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="main-content no-padding">
      <div class="full-image-section gray">
        <div class="row-fluid">
          <div class="image-container col-md-6 clearfix left">
            <div class="background-image-wrapper wow fadeInLeft"><img src="assets/img/feature/02.jpg" alt="background-image" class="background-image"></div>
          </div>
          <div class="container">
            <div class="col-md-6 col-md-offset-6 col-sm-12 col-sm-offset-0 no-padding clearfix">
              <div class="feature-content-page wow fadeInRight">
                <div class="content-title">
                  <h3>our history</h3>
                  <hr class="spacer">
                </div>
                <p> MAZ Global Services partners with a network of experienced Chartered Accountants, Company Secretaries, Advocates, Cost Accountants, and Financial Experts across India to provide a comprehensive range of services for small and medium enterprises.
                MAZ Global Services Ability to maintain a high level of accuracy of preparing and entering financial information.</p>
                <p> We are able to understand Client Quires and Assuring you our best services at all times.
                Our business value includes not only profitable but also customer satisfaction.</p>

              </div>
            </div>
          </div>
        </div>
      </div>

    </section>

    <footer id="footer">
      <div id="footer-widget" class="dark">
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-sm-6 clearfix">
              <h3>Corporate Office</h3>
              <aside class="widget">

                <div class="content fci">
                  <p>MAZ GLOBAL ACCOUNTING & LEGAL SERVICES (P) LTD NO: 59/118, MOORE STREET, PARRYS, CHENNAI - 01 PH:044 42083265 </br>www.mazglobalservices.com support@mazglobalservices.com</p>
                  <ul class="contact-info">
                    <li>
                      <p><i class="ion-ios-email"></i><span>Email :</span><a href="#">info@mazglobalservices.com</a></p>
                    </li>
                    <!--<li>
                      <p><i class="ion-ios-telephone"></i><span>phone :</span>+1 09 23 456</p>
                    </li>-->
                    <li>
                      <p><i class="ion-ios-location"></i><span>address :</span>Parries Corner, Chennai - 600 001.</p>
                    </li>
                  </ul>
                </div>
              </aside>
            </div>
            <div class="col-md-3 col-sm-6 clearfix">
                  <aside class="widget">
                    <div class="title">
                      <h3>Branches</h3>
                      <hr class="spacer">
                                          </div>

                     <div class="content">
                      <aside class="widget">

                        <div class="content fci">

                          <ul class="contact-info">
                            <li>
                              <p><i class="ion-ios-email"></i><span>Email :</span><a href="#">info@mazglobalservices.com</a></p>
                            </li>

                            <li>
                              <p><i class="ion-ios-location"></i><span>address :</span>No 21/11 Dhandauthapani first street Kotturpuram, Chennai - 600085.</p>
                            </li>
                          </ul>
                        </div>
                    </div>
                  </aside>
            </div>
            <div class="col-md-3 col-sm-6 clearfix">
                  <aside class="widget">
                    <div class="title">
                      <h3>Saidapet Office</h3>
                      <hr class="spacer">
                    </div>
                    <div class="content">
                      <aside class="widget">

                        <div class="content fci">

                          <ul class="contact-info">
                            <li>
                              <p><i class="ion-ios-email"></i><span>Email :</span><a href="#">info@mazglobalservices.com</a></p>
                            </li>

                            <li>
                              <p><i class="ion-ios-location"></i><span>address :</span>Saidapet, Chennai.</p>
                            </li>
                          </ul>
                        </div>
                    </div>
                  </aside>
            </div>

          </div>
        </div>
      </div>
      <div id="footer-bottom" class="darker">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-sm-6 clearfix">
              <p class="copyright">MAZ Global Services &copy; Designed &amp; Developed by<a href="http://www.mazglobalservices.com/it.html">MAZ Global Services</a></p>
            </div>

          </div>
        </div>
      </div>
    </footer>
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.inview.js"></script>
    <script type="text/javascript" src="assets/js/superfish.js"></script>
    <script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="assets/js/wow.min.js"></script>
    <script type="text/javascript" src="assets/js/owl.carousel.js"></script>
    <script type="text/javascript" src="assets/js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="assets/js/jquery.cubeportfolio.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.youtubebackground.js"></script>
    <script type="text/javascript" src="assets/js/jquery.backgroundVideo.js"></script>
    <script type="text/javascript" src="assets/js/flickr-feed.js"></script>
    <script type="text/javascript" src="assets/js/tweecool.js"></script>
    <script type="text/javascript" src="assets/js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="assets/js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.gmaps.js"></script>
    <script type="text/javascript" src="assets/js/localscroll.js"></script>
    <script type="text/javascript" src="assets/js/switcher/jquery.cookie.js"></script>
    <script type="text/javascript" src="assets/js/switcher/styleswitch.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>

  </body>

<!-- Mirrored from jogjafile.com/html/bala/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 08 Mar 2017 21:29:30 GMT -->
</html>