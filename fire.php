﻿<!DOCTYPE html>
<html lang="en" class="no-js">
  
<!-- Mirrored from jogjafile.com/html/bala/services.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 08 Mar 2017 21:30:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>FIRE & SAFETY LICENSE REGISTRATION :: MAZ Global Services</title>
    <link href="assets/stylesheets/vendor/bootstrap.css" rel="stylesheet" media="all" type="text/css">
    <link href="assets/stylesheets/vendor/ionicons.css" rel="stylesheet" media="all" type="text/css">
    <link href="assets/stylesheets/modules/animate.css" rel="stylesheet" media="all" type="text/css">
    <link href="assets/stylesheets/modules/flexslider.css" rel="stylesheet" media="all" type="text/css">
    <link href="assets/js/rs-plugin/css/settings.css" rel="stylesheet" media="all" type="text/css">
    <link href="assets/stylesheets/style.css" rel="stylesheet" media="all" type="text/css">
    <link href="assets/stylesheets/style.css" rel="stylesheet" media="all" data-name="skins" type="text/css">
    <link href="assets/stylesheets/switcher.css" rel="stylesheet" media="all" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css"><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script> <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.2/jquery.xdomainrequest.min.js"></script>
    <![endif]-->
  </head>
  <body class="fixHeader">
    <div id="preloader"><span></span><span></span><span></span><span></span><span></span></div>
    <header id="main_header" class="default clearfix">
      <div id="header-default" class="main-header">
        <div class="header-top">
          <div class="container">
            <div class="row">
              <div class="col-md-9 clearfix">
                <div class="contact-bar">
                                <ul class="contact-top border">
                                  <li>
                                    <p><i class="ion-android-mail"></i><span>Email:</span><a href="#">info@mazglobalservices.com</a></p>
                                  </li>
                                    <li>
                                        <p><i class="ion-android-phone-landscape"></i><span>Phone:</span><a
                                                href="#">044 48513690</a></p>
                                    </li>
                                  <li class="hidden-xs">
                                      <p><span><a href="http://mazglobalservices.com/user/index.php?rt=account/login">LOGIN</a></span></p>
                                  </li>
                                </ul>
                </div>
              </div>
              <div class="col-md-2 hidden-xs hidden-sm">
                <div class="social-bar pull-right">
                                <ul class="social ion-fonts">
                                    <li><a href="#" data-placement="bottom" data-toggle="tooltip" data-original-title="facebook" title="facebook" class="facebook"><i class="ion-social-facebook"></i></a></li>
                                  <li><a href="#" data-placement="bottom" data-toggle="tooltip" data-original-title="twitter" title="twitter" class="twitter"><i class="ion-social-twitter"></i></a></li>

                                </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="header-menu">
          <div class="container">
            <div id="menu_toggle_button"><i class="ion-navicon-round"></i></div>
            <div id="logo"><a href="#"><img src="assets/img/logo.png" alt="logos"></a></div>
            <div class="search-toggle"><a title="Search" class="search-icon"></a>
              <div id="search-container" class="search-box-wrapper">

              </div>
            </div><nav class="main-nav pull-right">
            <ul id="main_menu" class="menu clearfix">
               
                <li><a href="index.php">home</a>
                    <!--  <ul class="navi">
                        <li class="current-menu-item"><a href="index-2.html">home version 1</a></li>
                        <li><a href="index-3.html">home version 2</a></li>
                        <li><a href="index-4.html">home version 3</a></li>
                        <li><a href="index-5.html">home version 4</a></li>
                        <li><a href="index-6.html">home version 5</a></li>
                      </ul>-->
                </li>
                <li class="current-menu-item"><a href="#">Startup</a>
                    <div class="megaMenu three-quarter">
                        <div class="megaMenu-inner clearfix">
                            <ul>
                                <li><span>Business Registration</span>
                                    <ul>
                                        <li><a href="esi.php">ESI Registration</a></li>
                                        <li><a href="gst.php">GST Registration</a></li>
                                        <li><a href="ie.php">IE Code Registration</a></li>
                                        <li><a href="pf.php">PF Registration</a></li>
                                        <li><a href="tax.php">Professional TAX Registration</a></li>
                                        <li><a href="st.php">ST Registration</a></li>
                                        <li><a href="tin.php">TIN Registration</a></li>
                                        <li><a href="trademark.php">Trademark Registration</a></li>
                                    </ul>
                                </li>
                                <li><span>FIRM Registration</span>
                                    <ul>
                                        <li><a href="llp.php">LLP Registration</a></li>
                                        <li><a href="opc.php">OPC Registration</a></li>
                                        <li><a href="partnership.php">Partnership Registration</a></li>
                                        <li><a href="pvt.php">Private Limited Registration</a></li>
                                        <li><a href="society.php">Society Registration</a></li>
                                        <li><a href="ssi.php">SSI or MSME Registration</a></li>
                                        <li><a href="trust.php">Trust Registration</a></li>
                                    </ul>
                                </li>
                                <li><span>Licensing Services</span>
                                    <ul>
                                        <li><a href="apenda.php">Apeda Registration</a></li>
                                        <li><a href="dsc.php">DSC Registration</a></li>
                                        <li><a href="fire.php">Fire & Safety Registration</a></li>
                                        <li><a href="fssai.php">FSSAI License</a></li>
                                        <li><a href="spice.php">Spice Board Registration</a></li>
                                        <li><a href="tea.php">Tea Board Registration</a></li>
                                        <li><a href="trade.php">Trade License</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li><a href="#">Accounting & Complaince</a>
                    <ul class="navi">
                        <li><a href="add.php">Add Directors or Partner or Subscriber</a></li>
                        <li><a href="auditor.php">Auditor Appointment</a></li>
                        <li><a href="winding.php">Dissolution or Winding Up</a></li>
                        <li><a href="capital.php">Increase Authorized Capital</a></li>
                        <li><a href="name_change.php">Name Change</a></li>
                        <li><a href="register_office.php">Register Office Change</a></li>
                        <li><a href="remove.php">Remove Directors or Partner or Subscriber</a></li>
                        <li><a href="share_transfer.php">Share Transfer</a></li>
                    </ul>
                </li>
                <li class="current-menu-item"><a href="#">IXBRL</a>

                    <ul>

                        <li><a href="india_xbrl.php">India XBRL</a></li>
                        <li><a href="singapore_xbrl.php">Singapore XBRL</a></li>


                    </ul>

                </li>
              <li><a href="#">Legal Consulting</a>
                <!--  <ul>
                    <li><a href="contact.html">Style 1</a></li>
                    <li><a href="contact-2.html">Style 2</a></li>
                  </ul>-->
              </li>
              <li><a href="#">Software Solutions</a></li>
            </ul></nav>
          </div>
        </div>
      </div>
    </header>
    
    
    <section id="banner" data-parallax="assets/img/back.jpg" class="main-content banner-page register">
      <div class="container">
        <div class="banner-wrapper">
          <div class="bp-content">
            <h1 class="mazz-heading2">FIRE & SAFETY LICENSE REGISTRATION</h1>
            <p class="mazz-subhead">FIRE & SAFETY LICENSE REGISTRATION Starts From Rs.9,999/*</p>
          </div>
            <div id="message"></div>
            <article id="registerSubmit" class="form scrollpoint sp-effect5">
                <div>
                    <input type="text" placeholder="Name" name="name" id="name">
                    <span class="required" id="name_error"></span>
                </div>
                <div>
                    <input type="text" placeholder="Email" name="email" id="email">
                    <span class="required" id="email_error"></span>
                </div>
                <div>
                    <input type="text" placeholder="Mobile No" name="mobile" id="mobile">
                    <span class="required" id="mobile_error"></span>
                </div>
                <div>
                    <textarea type="text" placeholder="Address" name="address" id="address"></textarea>
                    <span class="required" id="address_error"></span>
                </div>
                <div>
                    <textarea type="text" placeholder="Reason" name="reason" id="reason"></textarea>
                    <span class="required" id="reason_error"></span>
                </div>
                <div>
                    <!--<label for="name"></label>-->
                    <input type="submit" name="submit" id="register_form" value="Submit">
                </div>
            </article>
           <!-- <div class="fm">
                <div class="form1">

                    <input type="text" name="to_name" class="form1-control customer-name" id="firstname"
                           placeholder="Name"
                           data-error="Enter Your First Name" required>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form1">

                    <input type="text" name="to_email" class="form1-control customer-email" id="inputEmail"
                           placeholder="Email"
                           data-error="This email address is invalid" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form1">

                    <input type="text" name="to_phoneno" class="form1-control customer-phoneno" id="Phone"
                           placeholder="Phone"
                           data-error="Enter Your Phone Number" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form1">
                        <textarea name="comment" form="usrform" class="form1-txt customer-message"
                                  placeholder="Message(Optional)"></textarea>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="submit" value="CONTACT US" class="button"
                           onclick="javascript:return validateform_reg();">
                </div>
            </div>-->
        </div>
      </div>
    </section>
    <div class="banner-breadcrumb default">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="breadcrumbs">
              <ol class="breadcrumb">
                <li class="labels">You are here:</li>
                <li><a href="#">Home</a></li>
                <li><a href="#">page</a></li>
                <li class="active">Our Services</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
    <section class="main-content">
      <div class="container">
        <div class="row">
          <div class="heading">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <h3>FIRE & SAFETY LICENSE REGISTRATION</h3>
              <hr class="spacer"/>
              <p style="width: 100%;
    margin: 0 auto;
    line-height: 1.85;
    color: #424242;
    font-weight: normal;
    font-size: 16px;
    font-family: sans-serif;">Fire Licence or NOC is needed under the following circumstances, to carry out businesses and trading activities, as listed under in the 
                Tamil Nadu Fire and Rescue Services Rules and obtain Building Plan Approval from the Chennai Metropolitan Development Authority for multi-storeyed buildings
                 like, Ice Manufacturing Industries, Crackers Manufacturing, Fire Works, Hotels and Restaurants Etc.</p>
            </div>
          </div>

      </div>
      </div>
    </section>

    <div class="pricing-sc section-sc" style="
    margin-top: -90px;">
        <div class="container">
            <div class="row">
                <div class="heading"  style="margin-bottom: 0px;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-md-offset-1">
                        <h3>OUR BEST PRICE</h3>
                        <hr class="spacer"/>
                                    </div>
                    <div class="col-lg-12" style="font-size: 18px; margin-bottom: 25px;">
                        <span>We offer three packages for the entire process of Fire & Safety License.</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-md-offset-1">
                    <div class="pricing-table special">
                        <div class="header">
                            <div class="title">MAZ – BASIC PRICE</div>
                            <div class="value"><span class="currency">Rs.</span><span class="price">9,999/-*</span><span class="schedul"></span></div>
                        </div>
                        <div class="content">
                            <ul>
                                <li><strong>Govt. fee</strong></li>
                                <li><strong>Processing fee</strong></li>
                                <li><strong>Fire & Safety License - Small Scale Industries</strong></li>
                                <li><strong>Documentation Charges</strong></li>
                               
                            </ul>
                        </div>
                        <div class="action" style="padding-bottom: 54px;"><a href="#" class="btn btn-line primary btn-lg" target="_blank">GET STARTED</a></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="pricing-table special">
                        <div class="header">
                            <div class="title">MAZ – STANDARD PRICE</div>
                            <div class="value"><span class="currency">Rs.</span><span class="price">12,499/-*</span><span class="schedul"></span></div>
                        </div>
                        <div class="content">
                            <ul>
                                <li><strong>Govt. fee</strong></li>
                                <li><strong>Processing fee</strong></li>
                                <li><strong>Fire & Safety License - Medium Scale Industries</strong></li>
                                <li><strong>Documentation Charges</strong></li>
                                
                            </ul>
                        </div>
                        <div class="action"><a href="#" class="btn btn-line primary btn-lg" target="_blank">GET STARTED</a></div>
                    </div>
                </div>
                  <div class="col-md-3">
                       <div class="pricing-table special">
                           <div class="header">
                               <div class="title">MAZ – OFFER PRICE</div>
                               <div class="value"><span class="currency">Rs.</span><span class="price">14,999/-*</span><span class="schedul"></span></div>
                           </div>
                           <div class="content">
                               <ul>
                                   <li><strong>Govt. fee</strong></li>
                                   <li><strong>Processing fee</strong></li>
                                   <li><strong>Fire & Safety License - Large Scale Industries</strong></li>
                                   <li><strong>Documentation Charges</strong></li>
                               </ul>
                           </div>
                           <div class="action" style="padding-bottom: 54px;"><a href="#" class="btn btn-line primary btn-lg" target="_blank">GET STARTED</a></div>
                       </div>
                   </div>

            </div>
            <div class="row">
                <div class="col-md-offset-1" style="font-size: 18px;">
                    <p>* Inclusive Of All Charges</p>
                    <p>* No Hidden Charges</p>
                </div>
            </div>
        </div>
    </div>


    <div class="table-sc section-sc">
        <div class="container">
            <div class="row">
                <div class="heading">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-md-offset-6" style="margin-top: 30px;">
                        <h3>LIST OF DOCUMENTS REQUIRED</h3>
                        <hr class="spacer"/>

                    </div>
                </div>
            </div>
            <div class="row" style="
    margin-top: -50px;">
                <div class="col-md-10 col-md-offset-1">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">

                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Business Name</td>

                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Business Activity</td>

                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Pan Card of Proprietor / Pan Card of the Company</td>

                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Address Proof of Proprietor / Each Partner / Each Director</td>

                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Identity Proof of Proprietor / Each Partner / Each Director</td>

                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Photo of the Proprietor / Each Partner / Each Director</td>

                            </tr>
                            <tr>
                                <td>7</td>
                                <td>Rental Agreement / Lease Deed / Sale deed</td>

                            </tr>
                            <tr>
                                <td>8</td>
                                <td>Property Tax Receipt</td>

                            </tr>
                            <tr>
                                <td>9</td>
                                <td>Blue Print of the Property</td>

                            </tr>
                            <tr>
                                <td>10</td>
                                <td>Contact Details</td>

                            </tr>
                            <tr>
                                <td>11</td>
                                <td>NOC [Non Objection Certificate from Landlord]</td>

                            </tr>
                            <tr>
                                <td>12</td>
                                <td>Sales Tax / Service Tax Registration Certificate</td>

                            </tr>
                            <tr>
                                <td>13</td>
                                <td>Partnership Deed (If Partnership Firm)</td>

                            </tr>
                            <tr>
                                <td>14</td>
                                <td>Registration Certificate (If Partnership Firm)</td>

                            </tr>
                            <tr>
                                <td>14</td>
                                <td>Incorporation Certificate (If Private Limited Companies)</td>

                            </tr>
                            <tr>
                                <td>15</td>
                                <td>MOA & AOA (If Private Limited Companies)</td>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <section class="main-content">
        <div class="container">
            <div class="row">
                <div class="heading">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-md-offset-2">
                        <h3>TURNAROUND TIME</h3>
                        <hr class="spacer"/>
                        <p style="width: 100%;
    margin: 0 auto;
    line-height: 1.85;
    color: #424242;
    font-weight: normal;
    font-size: 16px;
    font-family: sans-serif;"> The process will take up to 15-20 working days from the date of submission of all the
                            necessary documents and information.</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
<!--    <section class="main-content action-box standard bg-main">
      <div class="container">
        <div class="col-md-12">
          <div class="action-box-content">
            <h1>Fully Responsive Design & Retina Ready</h1><a href="#" class="btn btn-bg-white">purchase now</a>
          </div>
        </div>
      </div>
    </section>-->
    <footer id="footer">
        <div id="footer-widget" class="dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 clearfix">
                        <h3>Corporate Office</h3>
                        <aside class="widget">

                            <div class="content fci">
                                <p>MAZ GLOBAL ACCOUNTING & LEGAL SERVICES (P) LTD NO: 59/118, MOORE STREET, PARRYS, CHENNAI - 01 PH:044 42083265 </br>www.mazglobalservices.com support@mazglobalservices.com</p>
                                <ul class="contact-info">
                                    <li>
                                        <p><i class="ion-ios-email"></i><span>Email :</span><a href="#">info@mazglobalservices.com</a></p>
                                    </li>
                                    <!--<li>
                                      <p><i class="ion-ios-telephone"></i><span>phone :</span>+1 09 23 456</p>
                                    </li>-->
                                    <li>
                                        <p><i class="ion-ios-location"></i><span>address :</span>Parries Corner, Chennai - 600 001.</p>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class="col-md-3 col-sm-6 clearfix">
                        <aside class="widget">
                            <div class="title">
                                <h3>Branches</h3>
                                <hr class="spacer">
                            </div>

                            <div class="content">
                                <aside class="widget">

                                    <div class="content fci">

                                        <ul class="contact-info">
                                            <li>
                                                <p><i class="ion-ios-email"></i><span>Email :</span><a href="#">info@mazglobalservices.com</a></p>
                                            </li>

                                            <li>
                                                <p><i class="ion-ios-location"></i><span>address :</span>No 21/11 Dhandauthapani first street Kotturpuram, Chennai - 600085.</p>
                                            </li>
                                        </ul>
                                    </div>
                            </div>
                        </aside>
                    </div>
                    <div class="col-md-3 col-sm-6 clearfix">
                        <aside class="widget">
                            <div class="title">
                                <h3>Saidapet Office</h3>
                                <hr class="spacer">
                            </div>
                            <div class="content">
                                <aside class="widget">

                                    <div class="content fci">

                                        <ul class="contact-info">
                                            <li>
                                                <p><i class="ion-ios-email"></i><span>Email :</span><a href="#">info@mazglobalservices.com</a></p>
                                            </li>

                                            <li>
                                                <p><i class="ion-ios-location"></i><span>address :</span>Saidapet, Chennai.</p>
                                            </li>
                                        </ul>
                                    </div>
                            </div>
                        </aside>
                    </div>

                </div>
            </div>
        </div>
        <div id="footer-bottom" class="darker">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 clearfix">
                        <p class="copyright">MAZ Global Services &copy; Designed &amp; Developed by<a href="http://www.mazglobalservices.com/it.html">MAZ Global Services</a></p>
                    </div>

                </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.inview.js"></script>
    <script type="text/javascript" src="assets/js/superfish.js"></script>
    <script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="assets/js/wow.min.js"></script>
    <script type="text/javascript" src="assets/js/owl.carousel.js"></script>
    <script type="text/javascript" src="assets/js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="assets/js/jquery.cubeportfolio.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.youtubebackground.js"></script>
    <script type="text/javascript" src="assets/js/jquery.backgroundVideo.js"></script>
    <script type="text/javascript" src="assets/js/flickr-feed.js"></script>
    <script type="text/javascript" src="assets/js/tweecool.js"></script>
    <script type="text/javascript" src="assets/js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="assets/js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.gmaps.js"></script>
    <script type="text/javascript" src="assets/js/localscroll.js"></script>
    <script type="text/javascript" src="assets/js/switcher/jquery.cookie.js"></script>
    <script type="text/javascript" src="assets/js/switcher/styleswitch.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
    <script type="text/javascript" src="assets/js/contact.js"></script>
  </body>

<!-- Mirrored from jogjafile.com/html/bala/services.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 08 Mar 2017 21:30:36 GMT -->
</html>